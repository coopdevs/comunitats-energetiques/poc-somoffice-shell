import ibanLib from "iban";

export const formatIBAN = input => {
  const parts = ibanLib.printFormat(input, " ").split(' ')

  return [parts[0], '****', '****', '****', parts[4], parts[5]].join(' ')
}
