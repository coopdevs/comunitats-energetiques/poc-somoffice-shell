import { formatPrice } from "./helpers";

describe("helpers", () => {
  describe("formatPrice", () => {
    it("returns price formated", () => {
      expect(formatPrice(4.5)).toEqual("4,50€");
    });

    describe("when monthly frequency is specified", () => {
      it("returns price formated", () => {
        expect(formatPrice(4.5, { frequency: "monthly" })).toEqual("4,50€/mes");
      });
    });
  });
});
