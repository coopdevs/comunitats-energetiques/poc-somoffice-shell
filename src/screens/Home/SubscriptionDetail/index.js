import { toDate, format } from "date-fns";
import { useAsync } from "react-async-hook";
import { useParams } from "react-router-dom";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { Typography } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { BackButton } from "components/BackButton";
import Alert from "@material-ui/lab/Alert";

import React, { useState, Fragment } from "react";

import { Columns } from "components/layouts/Columns";
import { Column } from "components/layouts/Column";
import { Inline } from "components/layouts/Inline";
import { Stack } from "components/layouts/Stack";
import { Button } from "components/Button";
import { Spinner } from "components/Spinner";
import { getSubscriptionDetail } from "lib/api/subscriptions";

import { formatIBAN } from "lib/formatIBAN";
import { ChangeTariffModal } from "./ChangeTariffModal";
import { AdditionalDataModal } from "./AdditionalDataModal";
import { ChangeTariffModalBA } from "./ChangeTariffModalBA";
import { SubscriptionIcon } from "../SubscriptionIcon";
import { displayVatNumber } from "../../../lib/domain/somconnexio/auth";

const Field = ({ label, value }) => (
  <div>
    <div>{label}:</div>
    <div>
      <strong>{value}</strong>
    </div>
  </div>
);

const formatAddress = (data) => {
  const { address, zip_code, city } = data;

  return (
    <Fragment>
      <div>{address}</div>
      <div>
        {zip_code} {city}
      </div>
    </Fragment>
  );
};

// TODO remove grid elements
const HistoryItem = ({ item }) => (
  <Fragment>
    <ListItem>
      <Grid>
        <Grid item>
          <Typography display="inline" variant="overline">
            {format(toDate(item.date), "dd/MM/yyyy")}
          </Typography>
        </Grid>
        {item.action ? (
          <>
            <Grid item>
              <Typography display="inline">{item.action}</Typography>
            </Grid>
            <Grid item>
              <Typography
                display="inline"
                color="textSecondary"
                variant="body2"
              >
                {item.description}
              </Typography>
            </Grid>
          </>
        ) : (
          <Grid item>
            <Typography display="inline">{item.description}</Typography>
          </Grid>
        )}
      </Grid>
    </ListItem>
  </Fragment>
);

export const SubscriptionDetail = () => {
  const { t } = useTranslation();
  const { id } = useParams();
  const [currentModal, setCurrentModal] = useState(null);
  const subscription = useAsync(() => getSubscriptionDetail({ id }), [id]);

  const closeModal = () => setCurrentModal(null);

  if (subscription.loading) {
    return <Spinner />;
  }

  if (subscription.error) {
    return <Alert severity="error">{t("common.errors.request_failed")}</Alert>;
  }

  const data = subscription.result.data;
  const isChangeOutLandline = (data.available_operations.includes("ChangeTariffOutLandline"));

  return (
    <Fragment>
      <ChangeTariffModal
        isOpen={currentModal === "change_tariff"}
        onClose={closeModal}
        subscription={subscription.result.data}
      />
      <AdditionalDataModal
        isOpen={currentModal === "additional_data"}
        onClose={closeModal}
        subscription={subscription.result.data}
      />
      <ChangeTariffModalBA
        isOpen={currentModal === "change_tariff_BA"}
        onClose={closeModal}
        subscription={subscription.result.data}
      />
      <Box mb={6}>
        <Box mb={2}>
          <BackButton />
        </Box>
        <Inline spacing={3}>
          <SubscriptionIcon variant="big" type={data.subscription_type} />
          <Box display="flex" flexDirection="column" justifyContent="center">
            <Typography variant="h6">
              {t(`subscriptions.type.${data.subscription_type}`)}
            </Typography>
            <Typography color="textSecondary">{data.description}</Typography>
          </Box>
        </Inline>
      </Box>
      <Box mb={6}>
        <Columns spacing={4}>
          <Column>
            <Stack>
              <Field label={t("subscriptions.detail.name")} value={data.name} />
              <Field
                label={t("subscriptions.detail.vat_number")}
                value={displayVatNumber(data.vat_number)}
              />
              <Field
                label={t("subscriptions.detail.iban")}
                value={formatIBAN(data.iban)}
              />
            </Stack>
          </Column>
          <Column>
            <Stack>
              <Field
                label={t("subscriptions.detail.email")}
                value={data.email}
              />
              {data.address && (
                <Field
                  label={t("subscriptions.detail.address")}
                  value={formatAddress(data.address)}
                />
              )}
            </Stack>
          </Column>
        </Columns>
      </Box>
      {data.subscription_type === "mobile" && (
        <Box mb={4}>
          <Inline spacing={3}>
            <Button onClick={() => setCurrentModal("change_tariff")}>
              {t("subscriptions.detail.change_tariff")}
            </Button>
            <Button onClick={() => setCurrentModal("additional_data")}>
              {t("subscriptions.detail.additional_data")}
            </Button>
          </Inline>
        </Box>
      )}
      { isChangeOutLandline && (
        <Box mb={4}>
          <Button onClick={() => setCurrentModal("change_tariff_BA")}>
            {t("subscriptions.detail.change_tariff_BA")}
          </Button>
        </Box>
      )}
      <Box mb={4}>
        <Typography variant="button" color="textSecondary">
          {t(`subscriptions.detail.history`)}
        </Typography>
        <List>
          {data.services.map((item) => (
            <HistoryItem key={item.code} item={item} />
          ))}
        </List>
      </Box>
    </Fragment>
  );
};
