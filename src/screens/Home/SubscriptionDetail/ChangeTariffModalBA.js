import React, { useState, useEffect } from "react";
import { Modal } from "components/Modal";
import { Button } from "components/Button";
import { Spinner } from "components/Spinner";
import { Tiles } from "components/layouts/Tiles";
import { Text } from "components/Text";
import { Link } from "components/Link";
import { Trans, useTranslation } from "react-i18next";
import { noop } from "lib/fn/noop";
import { createTicket } from "lib/api/tickets";
import { Typography } from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";

const ModalContentSubmitted = ({ isHandleError, onClickClose }) => {
  const { t } = useTranslation();

  return (
    <Tiles spacing={2} columns={1}>
      {isHandleError ? (
        <Alert severity="error">{t("common.errors.request_failed")}</Alert>
      ) : (
        <Alert severity="success">
          {t("common.petition_received_with_email_confirmation")}
        </Alert>
      )}
      <Button onClick={onClickClose} fullWidth={false}>
        {t("common.close")}
      </Button>
    </Tiles>
  );
};

const ModalContentUnsubmitted = ({
  onSubmit,
  submit,
  onClickClose,
  onContinue,
}) => {
  const { t } = useTranslation();

  const [isSelected, setIsSelected] = useState(false);
  const [needsInformation, setNeedsInformation] = useState(true);

  const onClickContinue = async () => {
    let result = "success";
    onContinue();
    try {
      await submit();
    } catch (e) {
      console.log("e", e);
      result = "error";
    } finally {
      onSubmit(result);
    }
  };

  if (isSelected) {
    return (
      <Tiles spacing={2} columns={1}>
        <Alert severity="info">
          <Text>
            {t("subscriptions.detail.change_tariff_BA_modal.confirm")}
          </Text>
        </Alert>
        <Button onClick={() => onClickContinue()}>
          {t("common.continue")}
        </Button>
        <Button onClick={onClickClose}>{t("common.cancel")}</Button>
      </Tiles>
    );
  }
  if (needsInformation) {
    return (
      <Tiles spacing={2} columns={1}>
        <Typography align="center" variant="h5">
          {t("subscriptions.detail.change_tariff_BA_modal.title")}
        </Typography>
        <Text>
          {t("subscriptions.detail.change_tariff_BA_modal.description_a")}
        </Text>
        <div>
          <Trans i18nKey="subscriptions.detail.change_tariff_BA_modal.description_b">
            <Link
              target="_blank"
              to={"mailto:" + t("common.assistance_email")}
            />
          </Trans>
        </div>
        <Button
          fullWidth={false}
          onClick={() => {
            setNeedsInformation(false);
            setIsSelected(true);
          }}
        >
          {t("common.accept")}
        </Button>
      </Tiles>
    );
  }
};

const ModalContent = ({ onSubmittingStateChange, ...props }) => {
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [hasError, setHasError] = useState(false);

  useEffect(() => {
    onSubmittingStateChange(isSubmitting);
  }, [isSubmitting]);

  if (isSubmitted) {
    return <ModalContentSubmitted isHandleError={hasError} {...props} />;
  } else if (isSubmitting) {
    return <Spinner />;
  } else {
    return (
      <ModalContentUnsubmitted
        onContinue={() => setIsSubmitting(true)}
        onSubmit={(result) => {
          if (result === "error") setHasError(true);
          setIsSubmitting(false);
          setIsSubmitted(true);
        }}
        {...props}
      />
    );
  }
};

export const ChangeTariffModalBA = ({ isOpen, onClose, subscription }) => {
  const [shouldBlockModal, setShouldBlockModal] = useState(false);

  const submit = async () => {
    return createTicket({
      body: "-",
      subject: "-",
      meta: [
        {
          key: "ticket_type",
          value: "change_tariff_out_landline",
        },
        {
          key: "previous_service",
          value: subscription.active_product_code,
        },
        {
          key: "ref_contract",
          value: subscription.code,
        },
        {
          key: "landline_number",
          value: subscription.description,
        },
        {
          key: "service_address",
          value: subscription.address.address,
        },
        {
          key: "service_city",
          value: subscription.address.city,
        },
        {
          key: "service_zip",
          value: subscription.address.zip_code,
        },
        {
          key: "service_state",
          value: subscription.address.subdivision,
        },
      ],
    });
  };

  return (
    <Modal
      isOpen={isOpen}
      onClose={shouldBlockModal ? noop : onClose}
      showCloseButton={!shouldBlockModal}
    >
      <ModalContent
        onSubmittingStateChange={(isSubmitting) =>
          setShouldBlockModal(isSubmitting)
        }
        submit={submit}
        onClickClose={onClose}
      />
    </Modal>
  );
};
