import React, { useContext } from "react";

export const RequestChangeContext = React.createContext();
export const useRequestChangeContext = () => useContext(RequestChangeContext);
