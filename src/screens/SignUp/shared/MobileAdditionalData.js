import React from "react";
import { Text } from "components/Text";
import { Button } from "components/Button";
import { TextField } from "components/TextField";
import { Checkbox } from "components/Checkbox";
import { Tiles, TileSpan } from "components/layouts/Tiles";
import { RadioToggle } from "components/RadioToggle";
import { Condition } from "./Condition";
import { AddressPicker } from "./AddressPicker";
import { ApiSelect } from "./ApiSelect";
import { required } from "lib/form/validators";
import { getAvailableProviders } from "lib/api/availableProviders";
import { useTranslation } from "react-i18next";
import { PreviousOwnerFields } from "./PreviousOwnerFields";

export const MobileAdditionalData = () => {
  const { t } = useTranslation();

  return (
    <div>
      <Tiles columns={2} spacing={4}>
        <TileSpan span="all">
          <RadioToggle.FormField
            name="keep_number"
            leftLabel={t(
              "funnel.signup.data.steps.mobile_line_additional_data.keep_number_yes"
            )}
            leftValue={true}
            rightLabel={t(
              "funnel.signup.data.steps.mobile_line_additional_data.keep_number_no"
            )}
            rightValue={false}
          />
        </TileSpan>
        <Condition when="keep_number" is={true}>
          <PreviousOwnerFields />
          <TileSpan span="all">
            <ApiSelect
              name="previous_provider"
              validate={required}
              label={t(
                "funnel.signup.data.steps.mobile_line_additional_data.previous_provider"
              )}
              mapItem={item => ({ label: item.name, value: item.id })}
              query={() => getAvailableProviders({ category: "mobile" })}
            />
          </TileSpan>
          <TextField.FormField
            mask="000 00 00 00"
            name="phone_number"
            validate={required}
            label={t(
              "funnel.signup.data.steps.mobile_line_additional_data.phone_number"
            )}
          />
          <Checkbox.FormField
            name="is_prepaid"
            validate={required}
            label={t(
              "funnel.signup.data.steps.mobile_line_additional_data.is_prepaid"
            )}
          />
          <Condition when="is_prepaid" is={true}>
            <TileSpan span="all">
              <TextField.FormField
                validate={required}
                name="icc_donor"
                info={t('funnel.signup.data.steps.mobile_line_additional_data.icc_help_text')}
                label={t(
                  "funnel.signup.data.steps.mobile_line_additional_data.icc"
                )}
              />
            </TileSpan>
          </Condition>
        </Condition>
        <TileSpan span="all">
          <Checkbox.FormField
            validate={required}
            name="has_sim_card"
            label={t(
              "funnel.signup.data.steps.mobile_line_additional_data.has_sim_card"
            )}
          />
        </TileSpan>
        <Condition when="has_sim_card" is={false}>
          <TileSpan span="all">
            <AddressPicker
              validate={required}
              name="delivery_address"
              label={t(
                "funnel.signup.data.steps.mobile_line_additional_data.delivery_address"
              )}
            />
          </TileSpan>
        </Condition>
        <Condition when="has_sim_card" is={true}>
          <TileSpan span="all">
            <TextField.FormField
              validate={required}
              name="icc"
              label={t(
                "funnel.signup.data.steps.mobile_line_additional_data.icc"
              )}
              info={t('funnel.signup.data.steps.mobile_line_additional_data.icc_help_text')}
            />
          </TileSpan>
        </Condition>
        <div />
        <Button type="submit">{t("common.continue")}</Button>
      </Tiles>
    </div>
  );
};
