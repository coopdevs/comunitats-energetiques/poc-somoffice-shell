import React, { useEffect, useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import Box from "@material-ui/core/Box";
import { DialogTitle, IconButton, makeStyles } from "@material-ui/core";

const inIframe = (() => {
  try {
    return window.self !== window.top;
  } catch (e) {
    return true;
  }
})();

const useStyles = makeStyles(theme => ({
  paperScrollPaper: {
    maxHeight: `calc(100% - ${theme.spacing(3)})`,
  },
  paperFullWidth: {
    maxWidth: `calc(100% - ${theme.spacing(3)})`,
  },
  paper: {
    borderRadius: 0,
  }
}));

const CloseIcon = () => <svg width="18" height="19" viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg">
<line x1="17.3536" y1="1.35355" x2="0.382991" y2="18.3241" stroke="#3E3382"/>
<line x1="0.353553" y1="0.646447" x2="17.3241" y2="17.617" stroke="#3E3382"/>
</svg>


const boxStyles = {
  display: "flex",
  flex: 1,
  flexDirection: "column",
  overflow: "auto",
  px: [3, 8],
  py: [3, 5],
  paddingBottom: 8
};

export const Modal = ({
  isOpen,
  onClose,
  children,
  showCloseButton = true
}) => {
  const [scrollCorrection, setScrollCorrection] = useState(0);
  const styles = useStyles();

  useEffect(() => {
    if (!isOpen || !window.parentIFrame) {
      return;
    }

    window.parentIFrame.getPageInfo(obj => {
      setScrollCorrection(obj.scrollTop / 2);
    });
  }, [isOpen]);

  return (
    <Dialog
      classes={styles}
      style={{ transition: "top 100ms", top: scrollCorrection }}
      open={isOpen}
      onExited={onClose}
      onBackdropClick={onClose}
      fullWidth
    >
      <DialogTitle disableTypography>
        {showCloseButton && (
          <Box position="absolute" top={0} right={0} padding={2}>
            <IconButton onClick={onClose}>
              <CloseIcon />
            </IconButton>
          </Box>
        )}
      </DialogTitle>
      <Box {...boxStyles}>{children}</Box>
      <Box paddingBottom={4} />
    </Dialog>
  );
};
