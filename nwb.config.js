const path = require('path');

module.exports = {
  type: 'react-component',
  devServer: {
    host: '0.0.0.0',
    disableHostCheck: true,
    open: false,
    contentBase: path.resolve(__dirname, './demo/assets'),
  },
  npm: {
    esModules: true,
    umd: false
  },
  babel: {
    plugins: [
      ['module-resolver', {
        'root': ['src'],
      }],
    ],
  }
}
