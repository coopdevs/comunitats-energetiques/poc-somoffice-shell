# somoffice-shell

This is a proof of concept project of how to extract somoffice frontend into a
reusable customizable application.

To simplify this project was bootstrapped using
[nwb](https://github.com/insin/nwb), but I would look into an alternative (or just use [rollup](https://rollupjs.org/)) as
this project does not seem to be actively maintained lately. What's interesting
about **nwb** is that apart from configuring all necessary bundling for
you, it sets up a demo application where you can consume the component you are building.

The idea is to create a component that sets up the whole somoffice frontend, and allows customization.


```javascript

<Router>
  <SomofficeShell
    theme={/* instance of MaterialUI theme */}

    /* Allow inner components to be overriden/customized (see Customizing Components)*/
    components={{
      Button: CustomButton,
      Input: CustomInput,

      InvoicesIndexPage: CustomInvoicesPage
    }}

    translationsUrl={{
      es: "/path/to/custom-locale.es.json",
      ca: "/path/to/custom-locale.ca.json"
    }}
  />

  {/* Extra paths can also be defined through react-router */}
  <Route path="/custom-screen">
    <CustomScreenComponet />
  </Route>
</Router>

```


# Customizing components

We can customize components by leveraging on [React Context](https://reactjs.org/docs/context.html) as a means of dependency injection.

1. Components defined in the `components` prop of SomofficeShell component are put in a context.
2. When defining a custom component we use helper function `markAsCustomizable` (see src/lib/markAsCustomizable.js)

Using this mechanism we can be intentional about what can be customized and what not.

# Customizing theme

The idea is to use [MaterialUI theming mechanisms](https://mui.com/customization/theming/)

# Customizing translations

Additional keys can be downloaded and deep merged with the default ones.

- For this, some separation is necessary (some texts are now are exclusive to Som Connexio business logic).
- Add some interpolation like {brand_name}, so text with references to "Som Connexio" brand but in a generic context can be reused.


# Peer dependencies or not?

In `somoffice-shell` library we must at least define `react` and `react-dom` as [peer dependencies](https://nodejs.org/es/blog/npm/peer-dependencies/). This way this dependencies are not included in the final bundled, but are required to be installed by the host application.

It is yet to be defined what other dependencies might make sense to mark as peer dependencies (react-router, material-ui, etc...)
