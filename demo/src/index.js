import './mockApi'
import React, { Component } from "react";
import { render } from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";

import { SomofficeShell } from "../../src";

const CustomButton = () => <button>CustomButton</button>
const CustomLink = ({ children}) => <a>custom link: {children}</a>

export default class Demo extends Component {
  render() {
    return (
      <Router>
        <SomofficeShell
          components={{Button: CustomButton, Link: CustomLink}}
        />
      </Router>
    );
  }
}

render(<Demo />, document.querySelector("#demo"));
